var shoppingCart = JSON.parse(localStorage.getItem("user_session"));
var shoppingImgs = JSON.parse(localStorage.getItem("imgs_session"));
console.log(shoppingImgs);
console.log(shoppingCart);


loadItems();
loadShoppingCart();






function addShoppingCart(id) {
    console.log(id);
    var item = shoppingImgs.find((element) => {
        return element.id == id;
    });
    shoppingCart.shoppingCart.push(item);
    localStorage.setItem("user_session", JSON.stringify(shoppingCart));
    shoppingImgs = JSON.parse(localStorage.getItem("imgs_session"));
    alert("Se agrego un nuevo item al carrito, para un total de: " + JSON.parse(localStorage.getItem("user_session")).shoppingCart.length + " items");
    addToItemList(item);
}
function addToItemList(item) {
    $("#cleaningDiv").empty();
    var li = '<li>' + item.title + '   \$' + item.price + '</li>';
    var a = '<a onclick="cleanShoppingCart()">Clean shopping Cart</a>';
    $("#shoppingCartList").append(li);
    $("#cleaningDiv").append(a);
}
function loadItems() {
    shoppingImgs.forEach((img) => {
        var div = '<div class="col-md-3 col-xm-12"><img onclick="addShoppingCart(' + img.id + ')" src="' + img.url + '" alt="Smily face" height="250" width="250"><p>' + img.title + '  \$' + img.price + '</p></div>';
        $("#shoppingItem").append(div);
    })
}


function loadShoppingCart() {
    if (shoppingCart.shoppingCart.length > 0) {
        shoppingCart.shoppingCart.forEach((item) => {
            var li = '<li>' + item.title + '   \$' + item.price + '</li>';
            $("#shoppingCartList").append(li);
        });
        var a = '<a onclick="cleanShoppingCart()">Clean shopping Cart</a>';
        $("#cleaningDiv").append(a);
    } else {
        var p = '<p>Tu carrito está vacio</p>';
        $("#cleaningDiv").empty();
        $("#cleaningDiv").append(p);

    }

}

function cleanShoppingCart() {
    shoppingCart.shoppingCart = [];
    localStorage.setItem("user_session", JSON.stringify(shoppingCart));
    $("#shoppingCartList").empty();
    loadShoppingCart();
    alert("Se eliminaron los productos del carrito");
}
