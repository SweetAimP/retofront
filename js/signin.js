var userSession;
var users;
$(document).ready(function () {

    var user_username_email;
    var user_password;
    //datos para creación de usuario
    var id;
    var nombre;
    var username;
    var password;
    var mail;
    var telefono;

    if (!localStorage.getItem("user_data")) {
        getUsers();
    } else {
        users = localStorage.getItem("user_data");
        userSession = localStorage.getItem("user_session");
    }
    //Peticion para traer usuarios con un contenido predeterminado usando un json server https://my-json-server.typicode.com/.

    $("#btn_sign_in").click(function () {
        user_username_email = $("#login_username_email").val();
        user_password = $("#login_password").val();
        var userFound;
        // usuario de prueba  Bret Sincere@april.biz
        var data = JSON.parse(users);
        //validamos que los campos no esten vacios
        if (user_username_email != "" && user_password != "") {
            data.forEach((user) => {
                if (user.username === user_username_email || user.email === user_username_email) {
                    if (user.password === user_password) {
                        userFound = user;
                    }
                }
            });
            initSession(userFound);
        }
        else {
            alert("Ingresa todos los datos");
        }


    });

    $("#btn_sign_up").click(function () {
        $("#registry_section").show();
        $("#initial_section").hide();
    });

    $("#btn_register").click(function () {
        startRegistry();
    });
});
function getUsers(callback) {
    if (!!callback) {
        callback();
    } else {
        $.ajax('http://jsonplaceholder.typicode.com/users', {
            method: 'GET'
        }).then(function (data) {
            users = data;
            localStorage.setItem("user_data", JSON.stringify(data));
        });
    }
}
function initSession(userFound) {
    if (!!userFound) {
        var data = JSON.parse(userSession);
        data.name = userFound.name;
        localStorage.setItem("user_session", JSON.stringify(data));
        alert("Inicio de sesion exitoso");
        location.href = "index.html"
    } else {
        alert('Usuario o Correo incorrecto');
    }

}
function startRegistry() {
    var data = JSON.parse(users);
    id = data.length + 1;
    nombre = $("#user_name").val();
    username = $("#user_username").val();
    password = $("#user_password").val();
    email = $("#user_email").val();
    telefono = $("#user_phoneNumber").val();

    if (id != "" && nombre != "" && username != "" && password != "" && email != "" && telefono != "") {
        //creando el objeto usuario
        var usuario = {};
        usuario.id = id;
        usuario.name = nombre;
        usuario.username = username;
        usuario.password = password;
        usuario.email = email;
        usuario.phone = telefono;

        data.push(usuario);

        localStorage.setItem("user_data", JSON.stringify(data));
        users = localStorage.getItem("user_data");
        $("#initial_section").show();
        $("#registry_section").hide();
    } else {
        alert('completar todos los campos');
    }
}




