var imgs;
$(document).ready(function () {
    if (!localStorage.getItem("user_session")) {
        initSessionUserVar();
    } else {
        var data = JSON.parse(localStorage.getItem("user_session"));
        if (!!data.name) {
            $("#navSignIn").addClass("hide");
            var liUser = '<li id="userName"><a href="#">' + data.name + '</a></li>';
            var liSignOut = '<li id="signOut"><a onclick="logOut()" href="index.html">Sign Out</a></li>';
            $("#signinUl").append(liUser);
            $("#signinUl").append(liSignOut);
        }
    }

    if (!localStorage.getItem("imgs_session")) {
        getImgs();
    } else {
        imgs = localStorage.getItem("imgs_session");
        JSON.parse(imgs).forEach((element) => {
            var indicator = '<li data-target="#myCarousel" data-slide-to="' + element.id + '"></li>'
            var div = '<div class="item"><img alt ="slide ' + element.id + '" src="' + element.url + '"><div class="container"><div class="carousel-caption"><p>' + element.title +'  \$'+element.price +'</p><p><a class="btn addShoppingCartBtn" onclick="addShoppingCart(' + element.id + ')" role="button">Add to shopping cart</a></p></div></div></div>'

            $(".carousel-indicators").append(indicator);
            $(".carousel-inner").append(div);
        });
    }



});
function logOut() {
    alert('Se cerro sesion con exito');
    initSessionUserVar();
}

function getImgs() {
    $.ajax('https://jsonplaceholder.typicode.com/albums/1/photos', {
        method: 'GET'
    }).then(function (data) {
        imgs = data;
        imgs.forEach((img)=>{
            img.price = Math.floor((Math.random() * 10000) + 1000);
        });
        localStorage.setItem("imgs_session", JSON.stringify(data));
    });
}
function initSessionUserVar() {
    var userSession = {
        "name": "",
        "email": "",
        "shoppingCart":[]
    };
    localStorage.setItem("user_session", JSON.stringify(userSession));
}
